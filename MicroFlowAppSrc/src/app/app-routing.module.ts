import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './pages/login/login.component';
import { MicroFlowProductsComponent } from './pages/micro-flow-products/micro-flow-products.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { SetPasswordComponent } from './pages/set-password/set-password.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ForgotPasswordEmailSendComponent } from './pages/forgot-password-email-send/forgot-password-email-send.component';
import { BillingReportComponent } from './pages/billing-report/billing-report.component';

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {path: 'login',component: LoginComponent,},
  {path: 'emailsend',component:ForgotPasswordEmailSendComponent,},
  {path: 'forgotpassword',component:ForgotPasswordComponent,},

  {
    path: "",
    component: MenuComponent,
    children: [
      {path:'microflowproducts',component:MicroFlowProductsComponent,},
      {path:'menu',component:MenuComponent,},
      {path: 'setpassword',component:SetPasswordComponent,},
      {path: 'changepassword',component:ChangePasswordComponent,},
      {path:'billingreport',component:BillingReportComponent,},
    ],
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
