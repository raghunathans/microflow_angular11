import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  hide: boolean = true;
  hideNew: boolean = true;
  hideConfirm: boolean = true;

oldpassword:any="";
newpassword:any="";
confirmpassword:any="";
  constructor(

    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public notification:NotificationService,
    private service:ApiService
    ) 
    {

     }
     ChangePassword()
     {
       if (this.newpassword != this.confirmpassword) {
         this.notification.openSnackBar("Passwords do not match","","red-snackbar");
         return;
       }
       let url="api/UserAccount/SetPassword";
       let postData={
        UserEmail: localStorage.getItem("UserEmailVal"),
        SubscriptionID:this.oldpassword,
        Password:this.newpassword,
        AccessToken: localStorage.getItem("tokenval"),
        ChkChangePassword:"ChangePassword"
       }
       this.service.post(url,postData).subscribe(resp=>{
if(resp.status=='SUCCESS')
{
this.notification.openSnackBar("Password changed successfully","","success-snackbar");
this.dialogRef.close();
}
else if(resp.status=='FAILED')
  {
this.notification.openSnackBar(resp.message,"","warning-snackbar");
  }
},(error)=>{
  this.notification.openSnackBar(error.message,"","red-snackbar");
});
    }

  onCancel(): void {
    this.dialogRef.close();
  }


  ngOnInit(): void {
    
  }
  myFunction() {
    this.hide = !this.hide;
  }
  myFunctionNew() {
    this.hideNew = !this.hideNew;
  }
  myFunctionConfirm()
  {
    this.hideConfirm = !this.hideConfirm;
  }
}
