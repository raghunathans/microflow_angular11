import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from 'src/app/_api/api.service';
import { NotificationService } from 'src/app/notification.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormControl, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { Router } from '@angular/router';
import { TableUtil } from 'src/app/tableUtil';
 import * as XLSX from "xlsx";
 import {MatDatepickerModule} from '@angular/material/datepicker';
import { Users } from 'src/app/users';
import { Moment } from 'moment';
import * as moment from 'moment';


export interface viewsubscription{
ProductName:string;
SubscriptionName:string;
SubscriptionId:string;
TotalConsumption:number;
CurrentConsumption:Number;
PageAllotment:Number;
Overage:Number;
MaxOverage:Number;
UnusedPages:Number;
ActivationDate:string;
Status:string;
Action:any;
}
export interface ViewBillingDetails{
  BillID:string;
  BillDate:string;
  BillType:string;
  InvoiceNumber:string;
  InvoiceAmount:Number;
  OverageCount:Number;
  PaidStatus:number;
  Action:any;
  }
  export interface ChangePageCount{
    LoggedInUser:string;
    SubscriptionId:string;
    ChangeType:string;
    ChangeCount:number;
    CreatedOn:string;
    }
    export interface ConsumptionReport{
      Username:string;
      SubscriptionCode:string;
      PageCount:number;
      CreatedOn:string;
    }
    export interface InternalUserAction{
      UserName:string;
      Actions:string;
      ActionDescription:number;
      ActionDate:string;
    }
@Component({
  selector: 'app-micro-flow-products',
  templateUrl: './micro-flow-products.component.html',
  styleUrls: ['./micro-flow-products.component.scss'],
})
export class MicroFlowProductsComponent implements OnInit {
isShown:boolean=false;
// Users:string[]=[];
btninternalaction:boolean=false;
showuser:boolean=false;

btnAddSub:string="";
chnagetype:string="";
bonus:string="";
overage:string="";
changetype:string="";
txtbonus:any;
txtoverage:any;
reason:string="";
latestVersion:any;
myControl = new FormControl();
Users: Users[] = [];
lastkeydown1: number = 0;
subscription: any;
selectUser:any;
Check:string = "Hide";
showsearchuser:any;
viewInternalAudit:boolean=true;
btndisable:boolean=true;
downloadUrl:string="";
notify:boolean=true;
exportTable()
  {
    TableUtil.exportTableToExcel("ExampleMaterialTable", "billinginfo_");
  }
  exportConsumptionTable()
  {
    TableUtil.exportTableToExcel("ExampleMaterialTable", "consumptioninfo_");
  }
  // @ViewChild(MatPaginator) set paginator(pager: MatPaginator) {
  //   if (pager) this.dataSource.paginator = pager;
  // }
  // @ViewChild(MatSort) set sort(sorter: MatSort) {
  //   if (sorter) this.dataSource.sort = sorter;
  // }
  dataSource: MatTableDataSource<viewsubscription>;
  billingdataSource: MatTableDataSource<ViewBillingDetails>;
  changepagecountdatasource: MatTableDataSource<ChangePageCount>;
  consumptionReportdatasource:MatTableDataSource<ConsumptionReport>;
  viewInternalUserActionsdataSource:MatTableDataSource<InternalUserAction>;

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
@ViewChildren(MatSort) sort = new QueryList<MatSort>();

  displayedColumns: string[] = ['select','SubscriptionName', 'SubscriptionId', 'TotalConsumption','CurrentConsumption','PageAllotment',
  'Overage','MaxOverage','UnusedPages','ActivationDate','Status','Action'];

  billingdisplayedColumns: string[] = ['BillID', 'BillDate', 'BillType','InvoiceNumber','InvoiceAmount',
  'OverageCount','PaidStatus','Action'];
   changepagecountdisplayedColumns: string[] = ['LoggedInUser', 'SubscriptionId', 'ChangeType','ChangeCount','CreatedOn'];
   consumptiondisplayedColumns: string[] = ['Username', 'SubscriptionCode', 'PageCount','CreatedOn'];
   internalactiondisplayedColumns: string[] = ['UserName', 'Actions', 'ActionDescription','ActionDate'];

  constructor(private service:ApiService,private notification:NotificationService,private ngxLoader:NgxUiLoaderService,private router:Router) { 
    // this.dataSource=new MatTableDataSource();
    // this.billingdataSource=new MatTableDataSource();
    this.dataSource = new MatTableDataSource<viewsubscription>();
    this.billingdataSource = new MatTableDataSource<ViewBillingDetails>();
    this.changepagecountdatasource = new MatTableDataSource<ChangePageCount>();
    this.consumptionReportdatasource=new MatTableDataSource<ConsumptionReport>();
this.viewInternalUserActionsdataSource=new MatTableDataSource<InternalUserAction>();

  }

  ngOnInit(): void {
    if(localStorage.getItem('UserTypeval') == "Admin" || localStorage.getItem('UserTypeval') == "Internal")
    {
this.isShown=!this.isShown;
    }
    if(localStorage.getItem('UserTypeval') == "Admin")
    {
      this.btninternalaction=!this.btninternalaction;
    }
    
    this.latestVersion="Please download the latest version of the ScanSnap for Invoice client application. The old version will not work as of July 31 2022.If you have already downloaded the new version, please ignore this message.Link to download:"
    this.getViewSubscriptions();
    this.getUsers();
    this.getDownloadUrl();
    this.btndisable=true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.notify = false;
 }, 60000);
  }
  chkFlag:number=0;
start:string="";
to:string=";"
  BillingReport()
  {
    // this.BillingReportLoad();
    this.router.navigate(['/billingreport']);

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    
  }
  applyFilterBilling(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.billingdataSource.filter = filterValue.trim().toLowerCase();
    if (this.billingdataSource.paginator) {
      this.billingdataSource.paginator.firstPage();
    }
  }
  applyFilterChangePage(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.changepagecountdatasource.filter = filterValue.trim().toLowerCase();
    if (this.changepagecountdatasource.paginator) {
      this.changepagecountdatasource.paginator.firstPage();
    }
  }
  
  getDownloadUrl()
  {
    let url = "api/UserAccount/GetDownloadUrl";
    this.service.getAPI(url).then(resp => {
this.downloadUrl=resp;
    });
  }
  getViewSubscriptions()
  {
    let userId=localStorage.getItem('UserIdVal');
    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/UserAccount/MicroFlowViewSubscriptions?id=" + userId + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.user != null)
        {
         if(resp.user.length > 0)
         {
          this.dataSource = new MatTableDataSource(resp.user);
          this.dataSource.paginator = this.paginator.toArray()[0];
          this.dataSource.sort = this.sort.toArray()[0];    
          this.ngxLoader.stop();
         }
         else{
          resp.user=[];
          this.dataSource=resp.user;
          this.ngxLoader.stop();
         }
        }
        else{
          resp.user=[];
          this.dataSource=resp.user;
          this.ngxLoader.stop();
         }
      }
      else if(resp.status == "FAILED")
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  getUsers(){
    let token=localStorage.getItem('tokenval');
    let url = "api/UserAccount/GetAllUsers?token=" + token;
    this.service.getAPI(url).then(resp => {
      if (resp.status == "SUCCESS") {
        if (resp.userlist.length > 0) {
          this.Users=resp.userlist;
        }
        else{
          this.notification.openSnackBar("Users not found","","warning-snackbar");
        }
      }
      else if (resp.status == "FAILED") {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
      }
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  SubscriptionId:string="";

  ViewSubscriptionBilling(subCode:any)
  {
    this.SubscriptionId=subCode;
    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/Subscription/ViewSubscriptionBilling?SubscriptionCode=" + subCode + "&token=" + token + "&username="+localStorage.getItem('UserEmailVal') + "&useremail="+localStorage.getItem('EmailId')+"&usertype="+localStorage.getItem('UserTypeval');
    this.service.getAPI(url).then(resp => {
      if (resp.status == "SUCCESS") {
        if (resp.BillingList != null && resp.BillingList.length > 0) {
            for (var i = 0; i < resp.BillingList.length; i++) {
                if (resp.BillingList[i].PaidStatus == 0) {
                  resp.BillingList[i].PaidStatus = "Not Paid";
                }
                else {
                  resp.BillingList[i].PaidStatus = "Paid";
                }
            }
        
      this.billingdataSource = new MatTableDataSource(resp.BillingList);
      this.billingdataSource.paginator = this.paginator.toArray()[1];
      this.billingdataSource.sort = this.sort.toArray()[1]; 
      this.ngxLoader.stop();
        }
        else {
          resp.BillingList = [];
          this.billingdataSource=resp.BillingList;
          this.ngxLoader.stop();
      }
      }
      else if(resp.status == "FAILED")
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  onSelectRisk(row:any)
  {
    
  }
  ViewAuditTrail(subCode:any)
  {
    this.viewInternalAudit=false;
    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/Subscription/GetInternalAuditTrail?SubscriptionCode=" + subCode + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.InternalActions!=null)
        {
          if (resp.InternalActions.length > 0) {
            this.viewInternalUserActionsdataSource = new MatTableDataSource(resp.InternalActions);
            this.viewInternalUserActionsdataSource.paginator = this.paginator.toArray()[5];
            this.viewInternalUserActionsdataSource.sort = this.sort.toArray()[5];      
            this.ngxLoader.stop();
          }
        }       
        else{
          resp.InternalActions=[];
          this.viewInternalUserActionsdataSource=resp.InternalActions;
          this.ngxLoader.stop();

        }
      }
      else if(resp.status='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
   bonusvalue:number=0;

  ChangePage(subid:string,overage:number,bonus:number)
  {
    this.SubscriptionId=subid;
    this.bonusvalue=bonus;
    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/Subscription/GetChangePageCount?SubscriptionCode=" + subid + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        this.btnAddSub='Add';
        if(resp.ChangePageCount!=null)
        {
          if (resp.ChangePageCount.length > 0) {
            this.changepagecountdatasource = new MatTableDataSource(resp.ChangePageCount);
            this.changepagecountdatasource.paginator = this.paginator.toArray()[2];
            this.changepagecountdatasource.sort = this.sort.toArray()[2];      
            this.ngxLoader.stop();
          }
        }       
        else{
          resp.ChangePageCount=[];
          this.changepagecountdatasource=resp.ChangePageCount;
          this.ngxLoader.stop();

        }
      }
      else if(resp.status='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  SelectonChange(event: MatRadioChange)
  {
    if(event.value=='Overage')
    {
      this.btnAddSub='Subtract';
      this.txtbonus=null;
    }
    else{
      this.btnAddSub='Add';
this.txtoverage=null;
    }

  }
  AddAndSubPage()
  {
    if(this.changetype=='Bonus')
    {
      if(this.txtbonus=="" || this.txtbonus==null)
      {
        this.notification.openSnackBar("Please enter the value","","warning-snackbar");
        return;
      }
    }
    if(this.changetype=='Overage')
{
  if(this.txtoverage=="" || this.txtoverage==null)
  {
    this.notification.openSnackBar("Please enter the value","","warning-snackbar");
    return;
  }
}
this.txtbonus=parseInt(this.txtbonus);
if (this.bonusvalue + this.txtbonus > 60) {
  this.notification.openSnackBar("Bonus maximum limit should be 60.","","warning-snackbar");
  return;
}
if ((localStorage.getItem('EmailId') == null) || (localStorage.getItem('EmailId') == undefined))
{
  let email:any=localStorage.getItem('UserEmailVal');
  localStorage.setItem('EmailId',email);
}

this.ngxLoader.start();
let url="api/Subscription/ChangePageCount";
let postData={
  SubScriptionId:this.SubscriptionId,
  Bonus: this.txtbonus,
  Overage: this.txtoverage,
  AddPageOption: this.changetype,
  LoggedInUser:  localStorage.getItem('UserEmailVal'),
  AccessToken: localStorage.getItem('tokenval'),
  Username: localStorage.getItem('EmailId'),
  UserType:localStorage.getItem("UserTypeval"),
  Reason: this.reason
}
this.service.post(url,postData).subscribe(resp => {
  if(resp.status=='SUCCESS')
  {
  this.notification.openSnackBar("Page count changed successfully.","","success-snackbar");
  this.txtbonus='';
  this.txtoverage='';
  this.reason='';
  this.changetype='';
  this.Check = "Show";
  this.ngxLoader.stop();
   this.SearchUser(this.Check);

  }
  else if(resp.status=='FAILED')
  {
    this.ngxLoader.stop();
this.notification.openSnackBar(resp.message,"","warning-snackbar");
this.txtbonus='';
  this.txtoverage='';
  this.reason='';
  this.changetype='';

  }
},(error)=>{
  this.ngxLoader.stop();
this.notification.openSnackBar(error.message,"","red-snackbar");
this.txtbonus='';
  this.txtoverage='';
  this.reason='';
  this.changetype='';

});
  }
  onSelectConsumptionReport()
  {
    let token=localStorage.getItem('tokenval');

    let url = "api/Subscription/GetConsumptionReport?SubscriptionCode=" + this.SubscriptionId + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.ConsumptionReport!=null)
        {
          if (resp.ConsumptionReport.length > 0) {
            this.changepagecountdatasource = new MatTableDataSource(resp.ConsumptionReport);
            this.changepagecountdatasource.paginator = this.paginator.toArray()[3];
            this.changepagecountdatasource.sort = this.sort.toArray()[3];      
            this.ngxLoader.stop();
          }
        }       
        else{
          resp.ConsumptionReport=[];
          this.changepagecountdatasource=resp.ConsumptionReport;
          this.ngxLoader.stop();

        }
      }
      else if(resp.status='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  
  selecteduser:string="";
  ViewInternalActions()
  {
    this.viewInternalAudit=true;
    this.viewInternalUserActionsdataSource=new MatTableDataSource<InternalUserAction>();
    this.selecteduser="";
//     let token=localStorage.getItem('tokenval');
//     let url = "api/UserAccount/GetAllUsers?token=" + token;
//     this.service.getAPI(url).then(resp => {
// this.Users=resp.userlist;
//     });
    }
    selUser:string="";
    onUserChange(value: string) {
this.selUser=value;
    }
  SearchUserAction()
  {
    if(this.selUser=="")
    {
      this.notification.openSnackBar("Please select the user.","","warning-snackbar");
return;
    }



    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/UserAccount/GetInternalUserActions?username=" + this.selUser + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.InternalActions!=null)
        {
          if (resp.InternalActions.length > 0) {
            this.viewInternalUserActionsdataSource = new MatTableDataSource(resp.InternalActions);
            this.viewInternalUserActionsdataSource.paginator = this.paginator.toArray()[5];
            this.viewInternalUserActionsdataSource.sort = this.sort.toArray()[5];      
            this.ngxLoader.stop();
          }
        }       
        else{
          resp.InternalActions=[];
          this.viewInternalUserActionsdataSource=resp.InternalActions;
          this.ngxLoader.stop();

        }
      }
      else if(resp.status='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });

  }
Users1:string[]=[];
  getUserIdsFirstWay($event:any) {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.Users1 = [];

    if (userId.length > 2) {
      if ($event.timeStamp - this.lastkeydown1 > 200) {
        // this.Users1 = this.searchFromArray(this.Users, userId);
      }
    }
  }  
  searchFromArray(arr:any, regex:any) {
    let matches = [], i;
    for (i = 0; i < arr.length; i++) {
      if (arr[i].match(regex)) {
        matches.push(arr[i]);
      }
    }
    return matches;
  };
  chk:string="";
  SearchUser(chk:string)
  {
  this.Check=chk;
    if (this.Check != "Show") {
      if (localStorage.getItem("EmailId") == null || localStorage.getItem("EmailId") == "") {
          if (this.selectUser == null || this.selectUser == "" || this.selectUser == undefined) {
              this.notification.openSnackBar("Please enter the email address.","","warning-snackbar");
              return;
          }
      }
  }
  else {
      if (localStorage.getItem("EmailId") == null) {
        this.selectUser = localStorage.getItem("UserEmailVal")
      }
  }
  this.Check = "Hide";
  if (this.selectUser == localStorage.getItem("EmailId")) {
    if (localStorage.getItem("EmailId") != null) {
      this.selectUser = localStorage.getItem("EmailId");
    }
}
else if (this.selectUser == "" || this.selectUser == null) {
    if (localStorage.getItem("EmailId") != null) {
      this.selectUser = localStorage.getItem("EmailId");
    }
}
this.showsearchuser = this.selectUser;
this.showuser=true;


this.ngxLoader.start();
let url="api/UserAccount/SearchSubscriptionForUser";
let postData={
  UserEmail:this.selectUser,
  AccessToken: localStorage.getItem('tokenval'),
  Username:  localStorage.getItem('UserEmailVal'),
  UserType:localStorage.getItem("UserTypeval"),
}
this.service.post(url,postData).subscribe(resp => {
  if(resp.status=='SUCCESS')
      {
        if(resp.user != null)
        {
         if(resp.user.length > 0)
         {
          this.dataSource = new MatTableDataSource(resp.user);
          this.dataSource.paginator = this.paginator.toArray()[0];
          this.dataSource.sort = this.sort.toArray()[0];    
          this.ngxLoader.stop();
          localStorage.setItem("EmailId", this.selectUser);

         }
         else{
          resp.user=[];
          this.dataSource=resp.user;
          this.notification.openSnackBar("No subscriptions available for the user.","","warning-snackbar");
          this.ngxLoader.stop();
         }
        }
        else{
          resp.user=[];
          this.dataSource=resp.user;
          this.notification.openSnackBar("No subscriptions available for the user.","","warning-snackbar");
          this.ngxLoader.stop();
         }
      }
      else if(resp.status == "FAILED")
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
        localStorage.removeItem("EmailId");
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  selectedSubId:string="";
  setradio(subId:any)
  {
    this.selectedSubId=subId;
this.btndisable=false;
  }
CancelSubscription()
{
this.ngxLoader.start();
let url="api/Subscription/CancelSubscription";
let postData={
  SubscriptionId:this.selectedSubId,
  AccessToken: localStorage.getItem('tokenval')
}
this.service.post(url,postData).subscribe(resp => {
  if(resp.status=='SUCCESS')
      {  
        this.notification.openSnackBar("Subscription canceled successfully","","success-snackbar");
        this.getViewSubscriptions();
        this.btndisable=true;

          this.ngxLoader.stop();
      }
      else if(resp.status == "FAILED")
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
}
