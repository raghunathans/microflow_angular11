import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroFlowProductsComponent } from './micro-flow-products.component';

describe('MicroFlowProductsComponent', () => {
  let component: MicroFlowProductsComponent;
  let fixture: ComponentFixture<MicroFlowProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicroFlowProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroFlowProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
