import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { Router} from '@angular/router';
import { NgxUiLoaderService } from "ngx-ui-loader";


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  emailid:string="";
  newpassword:string="";
  confirmpassword:string="";
  hideNew: boolean = true;
  hideConfirm: boolean = true;

  constructor(private notification:NotificationService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService) { }

  ngOnInit(): void {
  }
  Forgotpassword()
  {
    if (this.newpassword != this.confirmpassword) {
      this.notification.openSnackBar("Passwords do not match","","red-snackbar");
      return;
    }
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                if (!this.emailid.match(validRegex)) {
                  this.notification.openSnackBar("Invalid email address","","warning-snackbar");

                    return;
                }
                var querystring;

                if (location.href && location.href.trim().length > 0 && location.href.indexOf('=') > 0)
                    querystring = location.href.split('=')[1];
                
                    let url="api/UserAccount/ForgotPassword";
                    let postData={
                      UserEmail: this.emailid,
                      Password:this.newpassword,
                      UserId:querystring,
                    }
                    this.ngxLoader.start();
             this.service.post(url,postData).subscribe(resp=>{
             if(resp.status=='SUCCESS')
             {
             this.notification.openSnackBar("Password changed successfully","","success-snackbar");
             this.router.navigate(['/login']);
             this.ngxLoader.stop();
             }
             else if(resp.status=='FAILED')
               {
             this.notification.openSnackBar(resp.message,"","warning-snackbar");
             this.ngxLoader.stop();
               }
               else{
                this.notification.openSnackBar("Password not set. User email is incorrect or time limit expired.","","warning-snackbar");
                this.ngxLoader.stop();
               }
             },(error)=>{
               this.notification.openSnackBar(error.message,"","red-snackbar");
               this.ngxLoader.stop();
             });
  }

  myFunctionNew() {
    this.hideNew = !this.hideNew;
  }
  myFunctionConfirm()
  {
    this.hideConfirm = !this.hideConfirm;
  }
}
