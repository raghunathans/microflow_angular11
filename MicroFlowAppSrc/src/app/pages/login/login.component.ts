import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,RouterOutlet } from '@angular/router';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
 import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  title = 'MicroFlowApp';
  recaptchaStatus: any;
  loginForm: FormGroup;
  validateLoginForm = false;
  authorizedUser = false;
  // hide = true;
  visible: boolean=false;
  isShown: boolean=false;
  hide = true;

  constructor(private fb: FormBuilder, private router: Router,private service:ApiService,
    private notification:NotificationService,
  private ngxLoader:NgxUiLoaderService
    ) {
      this.visible=false;
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],

    });
    
  }

  ngOnInit(): void {

  }
  get loginFormControls() {
    return this.loginForm.controls;
  }
  userSessionData: any;

  onSubmit() {
    localStorage.setItem('Username',this.loginForm.controls['username'].value);
    if (this.loginForm.invalid) {
      this.validateLoginForm = true;
      return;
    } else {
      this.validateLoginForm = false;
    }
    // var encrypted = this.service.encryptText(this.loginForm.controls['password'].value,'encryptemail');

    let loginData = {
      username: this.loginForm.controls['username'].value,
      password: this.loginForm.controls['password'].value,
    };
  //   if (loginData.password.indexOf('-') > -1) {
  //     loginData.password = loginData.password.replace(/-|\s/g, "");
  // }
      //  var encrypted = this.service.encryptText(this.loginForm.controls['password'].value,'encryptemail');
    //  var dec = this.service.decryptText(encrypted,'encryptemail');

    let data = {
      loginData: loginData,
    };
    let url = 'api/UserAccount/Login';
    this.ngxLoader.start();
    this.service.post(url,loginData).subscribe(resp=>{
      if(resp.status=="SUCCESS")
      {
                              localStorage.setItem("UserIdVal", resp.id);
                              localStorage.setItem("UserEmailVal", resp.email);
                              localStorage.setItem("tokenval", resp.token);
                              localStorage.setItem("UserTypeval", resp.usertype);
                              if (resp.flag == 0) {
                                this.router.navigate(['/setpassword']);
                                 this.ngxLoader.stop();
                            }
                            else {
                                localStorage.setItem("ShowLogoutval", "Enable");
                                this.router.navigate(['/microflowproducts']);
                                 this.ngxLoader.stop();
                              }
      
      }
      else if(resp.status=='FAILED')
      {
      this.notification.openSnackBar("Invalid Credentials","","red-snackbar")
       this.ngxLoader.stop();
      }
      },(error)=>{
        this.notification.openSnackBar(error.message,"","red-snackbar")
      });
  }
  CreateAccount($myParam: string = ''): void {
    this.router.navigate(['/createaccount']);
  }

  ForgotPassword()
  {
this.router.navigate(['/emailsend']);
  }
}
