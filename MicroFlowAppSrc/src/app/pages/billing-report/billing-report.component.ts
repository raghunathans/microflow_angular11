import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NotificationService } from 'src/app/notification.service';
import { TableUtil } from 'src/app/tableUtil';
import { ApiService } from 'src/app/_api/api.service';
export interface BillingReport{
  CustomerName:string;
  SubscriptionCode:string;
  BillID:string;
  InvDate:string;
  Status:string;
  BillDetail:string;
  Overage:number;
  InvoiceNumber:string;
  InvoiceAmount:number;
}
@Component({
  selector: 'app-billing-report',
  templateUrl: './billing-report.component.html',
  styleUrls: ['./billing-report.component.scss'],
  
})
export class BillingReportComponent implements OnInit {
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  fromdate:any;
  todate:any;
  maxDate:any;
  btnexport:boolean=true;
  billingReportdatasource:MatTableDataSource<BillingReport>;
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  

  billingReportdisplayedColumns: string[] = ['CustomerName', 'SubscriptionCode', 'BillID','InvDate','Status','BillDetail','Overage','InvoiceNumber','InvoiceAmount'];

  constructor(private service:ApiService,private notification:NotificationService,private ngxLoader:NgxUiLoaderService,private router:Router) { 
    this.billingReportdatasource=new MatTableDataSource<BillingReport>();
  }

  ngOnInit(): void {
    this.maxDate=new Date();
    this.BillingReportLoad();
  }
  exportBillingTable()
  {
    TableUtil.exportTableToExcel("ExampleMaterialTable", "Alluser_billinginfo_");
  }
  
  BillingReportLoad()
  {
    let start="";
    let to="";
    let token=localStorage.getItem('tokenval');
    this.ngxLoader.start();
    let url = "api/Subscription/GetBillingReport?fromdate=" + start +"&todate=" + to + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.BillingReportList!=null)
        {
          if (resp.BillingReportList.length > 0) {
            this.btnexport=false;
            this.billingReportdatasource = new MatTableDataSource(resp.BillingReportList);
            this.billingReportdatasource.paginator = this.paginator.toArray()[0];
            this.billingReportdatasource.sort = this.sort.toArray()[0];      
            this.ngxLoader.stop();
          }
        }       
        else{
          resp.BillingReportList=[];
          this.billingReportdatasource=resp.BillingReportList;
          this.ngxLoader.stop();

        }
      }
      else if(resp.status=='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
      }   
      else if(resp.status=='NORECORD')
      {
        this.ngxLoader.stop();
      }  
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  SearchBillingReport()
  {

    if(this.fromdate==null)
    {
      this.notification.openSnackBar("Please choose a fromdate.","","warning-snackbar");
      return;
    }
    if(this.todate==null)
    {
      this.notification.openSnackBar("Please choose a todate.","","warning-snackbar");
      return;
    }
     let start=this.convert(this.fromdate);
     let to=this.convert(this.todate);
    let token=localStorage.getItem('tokenval');

    let url = "api/Subscription/GetBillingReport?fromdate=" + start +"&todate=" + to + "&token=" + token;
    this.service.getAPI(url).then(resp => {
      if(resp.status=='SUCCESS')
      {
        if(resp.BillingReportList!=null)
        {
          if (resp.BillingReportList.length > 0) {
            this.btnexport=false;
            this.billingReportdatasource = new MatTableDataSource(resp.BillingReportList);
            this.billingReportdatasource.paginator = this.paginator.toArray()[0];
            this.billingReportdatasource.sort = this.sort.toArray()[0];      
            this.ngxLoader.stop();
            this.range.controls['start'].setValue("");
          this.range.controls['end'].setValue("");

          }
        }       
        else{
          resp.BillingReportList=[];
          this.billingReportdatasource=resp.BillingReportList;
          this.ngxLoader.stop();
          this.range.controls['start'].setValue("");
          this.range.controls['end'].setValue("");
          this.btnexport=true;

        }
      }
        else if (resp.status == "NORECORD") {
          resp.BillingReportList=[];
          this.billingReportdatasource=resp.BillingReportList;
          this.range.controls['start'].setValue("");
          this.range.controls['end'].setValue("");
          this.btnexport=true;
          this.ngxLoader.stop();

      }
      else if(resp.status=='FAILED')
      {
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
        this.ngxLoader.stop();
        this.range.controls['start'].setValue("");
          this.range.controls['end'].setValue("");
          this.btnexport=true;
      }    
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
      this.range.controls['start'].setValue("");
          this.range.controls['end'].setValue("");

    });
  }
   convert(str:any) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(),mnth,day].join("-");
  }

  Back()
  {
    this.router.navigate(['/microflowproducts']);
  }
}
