import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotPasswordEmailSendComponent } from './forgot-password-email-send.component';

describe('ForgotPasswordEmailSendComponent', () => {
  let component: ForgotPasswordEmailSendComponent;
  let fixture: ComponentFixture<ForgotPasswordEmailSendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotPasswordEmailSendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordEmailSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
