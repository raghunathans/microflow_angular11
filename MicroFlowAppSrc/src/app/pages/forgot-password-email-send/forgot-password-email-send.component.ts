import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/notification.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApiService } from 'src/app/_api/api.service';
import { Router} from '@angular/router'
@Component({
  selector: 'app-forgot-password-email-send',
  templateUrl: './forgot-password-email-send.component.html',
  styleUrls: ['./forgot-password-email-send.component.scss']
})
export class ForgotPasswordEmailSendComponent implements OnInit {
emailid:string="";
  constructor(private notification:NotificationService,private ngxLoader:NgxUiLoaderService,private service:ApiService,
    private router:Router) { 
      
    }

  ngOnInit(): void {

  }

  ForgotPasswordEmailSend()
  {
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                if (!this.emailid.match(validRegex)) {
                  this.notification.openSnackBar("Invalid email address","","warning-snackbar")
                    return;
                }
     let urlLink = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + "/MicroFlowAppNew/forgotpassword";

    this.ngxLoader.start();
    let url="api/UserAccount/SendForgotPasswordEmail";
    let postData={
      UserEmail:this.emailid,
      Url:urlLink,
    }
    
    this.service.post(url,postData).subscribe(resp => {
      if(resp.status=='SUCCESS')
      {
      this.notification.openSnackBar("Change password link sent to your email address.","","success-snackbar");
      this.emailid='';
      this.ngxLoader.stop();
    this.router.navigate(['/login']);
      }
      else if(resp.status=='FAILED')
      {
        this.ngxLoader.stop();
    this.notification.openSnackBar(resp.message,"","warning-snackbar");
    this.emailid='';
    
      }
    },(error)=>{
      this.ngxLoader.stop();
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.emailid='';
    
    });
    
  }
}
