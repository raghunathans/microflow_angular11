import { Component, OnInit, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { MatSidenav } from "@angular/material/sidenav";
import { Router } from "@angular/router";
import { ApiService } from 'src/app/_api/api.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ChangePasswordComponent } from 'src/app/pages/change-password/change-password.component';

// AWS.config.update({
//   accessKeyId: "AKIAZZAIYAQMUTF7KJO3",
//   secretAccessKey: "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",
//   region: "ap-south-1"
// });

// let s3 = new AWS.S3();

// const url = s3.getSignedUrl('getObject', {
//   Bucket: 'app-protectt-customer-logos',
//   Key: 'chola/cholams.png'
// });

// console.log(url)
// console.log(typeof(url));

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
})
export class MenuComponent implements OnInit {
  // public logoUrl: string = this.service.getCoockieLogo();
  path: any;
  // path = this.logoUrl;
  opened = true;
  over = "side";
  expandHeight = "42px";
  collapseHeight = "42px";
  displayMode = "flat";
  // overlap = false;
username:any;
  constructor(private router:Router,private service:ApiService,public dialog: MatDialog) {}
  // constructor(media:ObservableMedia) {
  //   this.watcher = media.subscribe((change: MediaChange) => {
  //     if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
  //       this.opened = false;
  //       this.over = 'over';
  //     } else {
  //       this.opened = true;
  //       this.over = 'side';
  //     }
  //   });
  // }

  ngOnInit() {
    this.username=localStorage.getItem('Username');
  }
  Logout() {

  let url="api/UserAccount/Logout";
let postData={
  AccessToken: localStorage.getItem('tokenval')
}
this.service.post(url,postData).subscribe(resp => {
  localStorage.removeItem("Redirectval");
  localStorage.removeItem("ShowLogoutval");
  localStorage.removeItem("tokenval");
  localStorage.removeItem("Prevstateval");
  localStorage.removeItem("UserEmailVal");
  localStorage.removeItem("UserIdVal");
  localStorage.removeItem("UserTypeval");
  localStorage.removeItem("EmailId");
  this.router.navigate(['/login']);

});
  }
  close(reason: string) {
    console.log(reason);
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '1000px',
      height:'500px',
      data: {  }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
    });
  }
}
