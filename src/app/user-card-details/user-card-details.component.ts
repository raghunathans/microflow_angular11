import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CookieModule, CookieService } from 'ngx-cookie';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NavbarService } from '../navbar.service';
import { NotificationService } from '../notification.service';
import { ApiService } from '../_api/api.service';

@Component({
  selector: 'app-user-card-details',
  templateUrl: './user-card-details.component.html',
  styleUrls: ['./user-card-details.component.css']
})
export class UserCardDetailsComponent implements OnInit {
  cardno:any;
  cvvpin:any;
  expdate:any;
  address:any;
  name:any;
  constructor(private fb:FormBuilder,
    private service:ApiService,
    public cookieserivice:CookieService,
    private router:Router,
    public nav:NavbarService,
    private snackBar:MatSnackBar,
    private notification:NotificationService,
    private ngxLoader:NgxUiLoaderService
    ) { }

  ngOnInit(): void {
    this.nav.navbarshowhide();
    let userSessionId=this.service.getCoockie()
    var val=userSessionId;
  // userSessionId = this.service.decryptText('encryptemail',userSessionId);
  var decrypted = this.service.decryptText('encryptemail', val);


  }
onClickSave()
{
  if((this.cardno==undefined || this.name==undefined || this.address==undefined || this.expdate==undefined)||(this.cardno=="" || this.name=="" || this.address=="" || this.expdate==""))
  {
    this.notification.openSnackBar("Please enter mandatory fields","","warning-snackbar");
return;
  }
  this.ngxLoader.start();
let url="api/UserAccount/GetSubscriptionId";
let postData={
  CardNumber:this.cardno,
  UserEmail: this.service.getCoockie(),
  ProductId: "1",
  AccessToken: "",
  PartNumber:     this.cookieserivice.get("partno")  ,
  CustomerName: this.service.getCoockie(),
  ContactEmail: this.service.getCoockie(),
  ActivationDate:new Date(),
  CardName: this.name,
  Address: this.address,
  ExpiryDate: this.expdate,
  CVV: "",
  TestApp: "Test123",
  UserType: "User"
}
this.service.insertSubscription(url,postData).subscribe(resp => {
  if(resp.status=='SUCCESS')
  {
    this.ngxLoader.stop();
  this.notification.openSnackBar("Subscription created successfully","","success-snackbar");
this.router.navigate(['/success'])
  }
  else if(resp.status=='FAILED')
  {
    this.ngxLoader.stop();
this.notification.openSnackBar(resp.message,"","warning-snackbar");
  }
},(error)=>{
  this.ngxLoader.stop();
this.notification.openSnackBar(error.message,"","red-snackbar");
});
}
mychange(val:any) {
  const self = this;
  let chIbn = val.split('-').join('');
  if (chIbn.length > 0) {
    chIbn = chIbn.match(new RegExp('.{1,4}', 'g')).join('-');
  }
  this.cardno = chIbn;
}
}
