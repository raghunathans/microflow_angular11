import { Injectable } from '@angular/core';

@Injectable()
export class NavbarService {
  visible: boolean;

  constructor() { this.visible = false; }

  hide() { this.visible = false; }

  show() { this.visible = true; }

  toggle() { this.visible = !this.visible; }

navbarshowhide() { 
if((localStorage.getItem("UserId")!="") && (localStorage.getItem("UserId")!=null))
{
this.show();
var username:any=localStorage.getItem("UserEmail");
}
else{
  this.hide();
}
return username;
  }
}