import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {
  private id: string="";
  
  constructor(private httpClient:HttpClient) 
  {
    
  }
  getAllProducts(headers:any):Observable<Product[]>
  {
   return this.httpClient.get<Product[]>("http://localhost/MicroflowApi/api/UserAccount/MicroFlowProducts",{ 'headers':headers });
  }
}
