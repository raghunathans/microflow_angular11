import { Component, OnInit,Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavbarService } from './navbar.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogData } from './notification.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  isShown: boolean=false;
username:string | null | undefined;
  constructor(private router:Router,public nav: NavbarService,
    public dialog: MatDialog) {}

  ngOnInit(): void {

    // if(window.location.pathname!='/login')
    // {
    //   this.isShown=true;
    // }
  this.username=  this.nav.navbarshowhide();
  }
  Logout()
  {
this.router.navigate(['/login']) 
localStorage.clear();
}
openDialog(): void {
  let dialogRef = this.dialog.open(ChangePasswordComponent, {
    width: '1000px',
    height:'500px',
    data: {  }
  });

  dialogRef.afterClosed().subscribe(result => {
    // this.animal = result;
  });
}


}
