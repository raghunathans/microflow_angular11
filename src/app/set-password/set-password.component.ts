import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';
import { ApiService } from '../_api/api.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {
  currentpassword:string="";
  newpassword:string="";
  confirmpassword:string="";
  constructor(
    public notification:NotificationService,
    private service:ApiService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }
  Setpassword()
  {
    let password = this.currentpassword;
    if (password.indexOf('-') > -1) {
        this.currentpassword = password.replace(/-|\s/g, "");
    }
    if (this.newpassword != this.confirmpassword) {
      this.notification.openSnackBar("Passwords do not match","","red-snackbar");
        return;
    }
    let url="api/UserAccount/SetPassword";
    let postData={
     UserEmail:localStorage.getItem("UserEmail"),
     SubscriptionID:this.currentpassword,
     Password:this.newpassword,
     AccessToken:localStorage.getItem("token")
    }
    this.service.post(url,postData).subscribe(resp=>{
if(resp.status=='SUCCESS')
{
this.notification.openSnackBar("Password set successfully","","success-snackbar");
this.router.navigate(['/login'])
}
else if(resp.status=='FAILED')
{
this.notification.openSnackBar(resp.message,"","warning-snackbar");
}
},(error)=>{
this.notification.openSnackBar(error.message,"","red-snackbar");
});
  }
  
}
