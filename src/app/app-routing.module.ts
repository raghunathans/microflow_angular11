import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAccountComponent } from './create-account/create-account.component';
import { LoginComponent } from './login/login.component';
import { MicroFlowProductsComponent } from './micro-flow-products/micro-flow-products.component'; 
import { UserCardDetailsComponent } from './user-card-details/user-card-details.component';
import { SuccessComponent } from './success/success.component';
import { SetPasswordComponent } from './set-password/set-password.component';

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {path: 'login',component: LoginComponent,},
  {path: 'createaccount',component: CreateAccountComponent,},
  {path: 'microflowproducts',component:MicroFlowProductsComponent,},
  {path: 'usercarddetails',component:UserCardDetailsComponent},
  {path: 'success',component:SuccessComponent,},
  {path: 'setpassword',component:SetPasswordComponent,},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
