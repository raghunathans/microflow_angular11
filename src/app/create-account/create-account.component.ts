import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { NavbarService } from '../navbar.service';
import { ApiService } from '../_api/api.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { NotificationService } from '../notification.service';

//  import { CookieService } from 'ngx-cookie';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})

export class CreateAccountComponent implements OnInit {
  // email:string="";
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
      
  }
  constructor(
     private router:Router,
     private service:ApiService,
    public cookie:CookieService,
    public nav: NavbarService,
public snackBar:MatSnackBar,
public notificationservice:NotificationService
  ) { }

  ngOnInit(): void {
    // this.nav.show();
    //  this.nav.doSomethingElseUseful();
   localStorage.removeItem("UserId");
  }
   onClickNext() {
     if(this.email.status=='INVALID')
     {
       return;
     }
    if (this.email.value=="") {
      this.notificationservice.openSnackBar("Please enter the email address","","red-snackbar");
      // alert("Please enter email address");
      return;
    }
     var encrypted = this.service.encryptText('encryptemail', this.email.value);
     var dec = this.service.decryptText('encryptemail', encrypted);
     this.service.setCoockie(this.email.value);
    this.router.navigate(['/microflowproducts'])
   }
  
 Back()
 {
  this.router.navigate(['/login'])
 }
}

