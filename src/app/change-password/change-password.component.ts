import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../notification.service';
import { ApiService } from '../_api/api.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  hide = true;
  showHideClass:any='fa fa-eye';
  showHideClassAuth:any = 'fa fa-eye';
  inputType:any = 'password';
  inputTypeval:any = 'password';

oldpassword:any="";
newpassword:any="";
confirmpassword:any="";
  constructor(

    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public notification:NotificationService,
    private service:ApiService
    ) 
    {

     }
     ChangePassword()
     {
       if (this.newpassword != this.confirmpassword) {
         this.notification.openSnackBar("Passwords do not match","","red-snackbar");
         return;
       }
       let url="api/UserAccount/SetPassword";
       let postData={
        UserEmail:localStorage.getItem("UserEmail"),
        SubscriptionID:this.oldpassword,
        Password:this.newpassword,
        AccessToken:localStorage.getItem("token")
       }
       this.service.post(url,postData).subscribe(resp=>{
if(resp.status=='SUCCESS')
{
this.notification.openSnackBar("Password changed successfully","","success-snackbar");
this.dialogRef.close();
}
else if(resp.status=='FAILED')
  {
this.notification.openSnackBar(resp.message,"","warning-snackbar");
  }
},(error)=>{
  this.notification.openSnackBar(error.message,"","red-snackbar");
});
    }

  onCancel(): void {
    this.dialogRef.close();
  }


  ngOnInit(): void {
    
  }

}
