import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { CookieService } from "ngx-cookie";
import { environment } from 'src/environments/environment';
import * as CryptoJS from "crypto-js";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  hostUrl:boolean=false;
  displayProgressSpinner: boolean=false;

  constructor(private http: HttpClient,
    public cookieService: CookieService,

    ) { }
  public url = environment.hostUrl;

  getAPI(url: string): Promise<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        "TokenApiKey":"KpL5QhIeR76Yc72pEeq9OZVpjrW6yQxy"
      }),
    };

    return new Promise((resolve, reject) => {
      this.http.get(this.url + url, httpOptions).subscribe(
        (result) => {
          resolve(JSON.parse(JSON.stringify(result)));
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  //POST method

  postAPI(url:string, postData:any): Promise<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
      }),
    };

    return new Promise((resolve, reject) => {
      this.http.post(this.url + url, postData, httpOptions).subscribe(
        (result) => {
          return result;
          // resolve(JSON.parse(JSON.stringify(result)));
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  post(url:string, postData:any)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
      }),
    };
    return this.http.post<any>(this.url+url,postData);

  }
  insertSubscription(url:string,postData:any):Observable<any>
  {
return this.http.post<any>(this.url+url,postData);
  }
  //Set and Get Cookiee

  setCoockie(data:any) {
    this.cookieService.put("userSessionId", data);
  }
  getCoockie() {
    console.log(this.cookieService.get("userSessionId"))
    return this.cookieService.get("userSessionId");
  }
  deleteCoockie() {
    this.cookieService.remove("userSessionId");
  }

   //Encryption
   encryptText(value:any, secretkey:any) {
    let key = CryptoJS.enc.Utf8.parse(secretkey);
    let iv = CryptoJS.enc.Utf8.parse(secretkey);
    let encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(value.toString()),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );
    return encrypted.toString();
  }
  //Decryption
  decryptText(value:any, secretkey:any) {
    let key = CryptoJS.enc.Utf8.parse(secretkey);
    let iv = CryptoJS.enc.Utf8.parse(secretkey);
    let decrypted = CryptoJS.AES.decrypt(value, key, {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }


  storeTmp: any;

  setData(val:any) {
    this.storeTmp = val;
  }

  getData() {
    if (this.storeTmp) return this.storeTmp;
    else return null;
  }

  clearData() {
    this.storeTmp = null;
  }
  
  showProgressSpinner = () => {
    this.displayProgressSpinner = true;
    setTimeout(() => {
      this.displayProgressSpinner = false;
    }, 3000);
  };
  
}

