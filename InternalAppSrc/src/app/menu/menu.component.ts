import { Component, OnInit, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { MatSidenav } from "@angular/material/sidenav";
import { Router } from "@angular/router";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { NotificationService } from 'src/app/notification.service';
import { ChangePasswordComponent } from 'src/app/pages/change-password/change-password.component';

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
})
export class MenuComponent implements OnInit {
  // public logoUrl: string = this.service.getCoockieLogo();
  path: any;
  // path = this.logoUrl;
  opened = true;
  over = "side";
  expandHeight = "42px";
  collapseHeight = "42px";
  displayMode = "flat";
  isbtnvisible:boolean = false;
  username:any;
  constructor(private router:Router,
    public dialog: MatDialog) {}
  // constructor(media:ObservableMedia) {
  //   this.watcher = media.subscribe((change: MediaChange) => {
  //     if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
  //       this.opened = false;
  //       this.over = 'over';
  //     } else {
  //       this.opened = true;
  //       this.over = 'side';
  //     }
  //   });
  // }

  ngOnInit() {
    if(localStorage.getItem('UserEmail')!=null && localStorage.getItem('UserEmail')!="")
    {
      this.username=localStorage.getItem('UserEmail');
this.isbtnvisible=true;
    }
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '1000px',
      height:'500px',
      data: {  }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
    });
  }
  Logout() {
    this.router.navigate(['/login']);
  }
  close(reason: string) {
    console.log(reason);
  }
}
