import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/_api/api.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { NotificationService } from 'src/app/notification.service';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  title = 'MicroFlowInternalApp';
  recaptchaStatus: any;
  loginForm: FormGroup;
  validateLoginForm = false;
  authorizedUser = false;
  // hide = true;
  visible: boolean=false;
  isShown: boolean=false;
  
  hide = true;

  constructor(private fb: FormBuilder, private router: Router,
    private service:ApiService,
    private notification:NotificationService,
    private ngxLoader: NgxUiLoaderService

    ) {
      this.visible=false;
      
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],

    });
    
  }

  ngOnInit(): void {
        setTimeout(() => {}, 0.01);

  }
  get loginFormControls() {
    return this.loginForm.controls;
  }
  userSessionData: any;

  onSubmit() {
    if (this.loginForm.invalid) {
      this.validateLoginForm = true;
      return;
    } else {
      this.validateLoginForm = false;
    }
    // var encrypted = this.service.encryptText(this.loginForm.controls['password'].value,'encryptemail');
    this.ngxLoader.start();

    let loginData = {
      username: this.loginForm.controls['username'].value,
      password: this.loginForm.controls['password'].value,
    };
  //   if (loginData.password.indexOf('-') > -1) {
  //     loginData.password = loginData.password.replace(/-|\s/g, "");
  // }
      //  var encrypted = this.service.encryptText(this.loginForm.controls['password'].value,'encryptemail');
    //  var dec = this.service.decryptText(encrypted,'encryptemail');

    let data = {
      loginData: loginData,
    };
    let url = 'api/UserAccount/Login';
this.service.post(url,loginData).subscribe(resp=>{
if(resp.status=="SUCCESS")
{
                        localStorage.setItem("UserId", resp.id);
                        localStorage.setItem("UserEmail", resp.email);
                        localStorage.setItem("token", resp.token);
                        localStorage.setItem("UserType", resp.usertype);
                        if (resp.flag == 0) {
                          // $state.go('app.SetPassword');
                          this.router.navigate(['/setpassword']);
                          this.ngxLoader.stop();
                      }
                      else {
                          localStorage.setItem("ShowLogout", "Enable");
                          this.router.navigate(['/microflowproducts']);
                          this.ngxLoader.stop();
                        }

}
else if(resp.status=='FAILED')
{
this.notification.openSnackBar("Invalid Credentials","","red-snackbar")
this.ngxLoader.stop();
}
});
  }
  CreateAccount($myParam: string = ''): void {
    if(localStorage.getItem("UserEmail")!="")
    {
      localStorage.removeItem("UserEmail");
    }
    this.router.navigate(['/createaccount']);
  }


}
