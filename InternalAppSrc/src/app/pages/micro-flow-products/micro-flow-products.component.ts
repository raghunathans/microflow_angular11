import { HttpHeaders } from '@angular/common/http';
import { ThisReceiver } from '@angular/compiler';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/notification.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
// import { ExecSyncOptionsWithStringEncoding } from 'child_process';
// import * as internal from 'stream';
import { Product } from 'src/app/product';
// import { ProductsService } from '../products.service';
import { ApiService } from 'src/app/_api/api.service';
export interface Subscription {
  Name: string;
  Description: string;
  Duration: string;
  PartNumber: string;
  Action:any;
}
@Component({
  selector: 'app-micro-flow-products',
  templateUrl: './micro-flow-products.component.html',
  styleUrls: ['./micro-flow-products.component.scss']
})
export class MicroFlowProductsComponent implements OnInit {
isShown: boolean=false;
isbtnvisible:boolean = false;
showproduct:boolean=true;
email:string="";
usertype:string="";
year:any;
btnproductdisable:boolean=false;
pagecount:string="";
selectedValue:any="";
Users:any[]=[];

  @ViewChild(MatPaginator) set paginator(pager: MatPaginator) {
    if (pager) this.dataSource.paginator = pager;
  }
  @ViewChild(MatSort) set sort(sorter: MatSort) {
    if (sorter) this.dataSource.sort = sorter;
  }
  dataSource: MatTableDataSource<Subscription>;
  displayedColumns: string[] = ['Name', 'Description', 'Duration', 'PartNumber','Action'];
  subscriptionList: any;
  createusernew:boolean=false;
  products: Product[] = [];
  private id: string = "";

  constructor(private service: ApiService,private router:Router,
    private notification:NotificationService,
    private ngxLoader:NgxUiLoaderService,
    ) {
   this.dataSource=new MatTableDataSource();
  }

  username:string="";
  ngOnInit(): void {
    this.getAllProducts();
    if(localStorage.getItem('UserType')=='Admin')
    {
this.isbtnvisible=true;
this.btnproductdisable=true;
this.createusernew=false;
    }
    this.year=new Date().getFullYear();

    if(localStorage.getItem('UserType')=='Admin')
    {
this.showproduct=false;
    }
  }
  getAllProducts() {
    this.ngxLoader.start();
    let url = "api/UserAccount/MicroFlowProducts?id=" + "";
    this.service.getAPI(url).then(resp => {
      this.products = resp;
      this.ngxLoader.stop();
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  val:string="";
  onProductChange(value: string) {
    this.val=value;
    if(this.val=="")
    {
      this.isShown =! this.isShown;
      return;
    }
    this.isShown = ! this.isShown;
    var userid = localStorage.getItem("UserId");

    if (userid == "" || userid == undefined) {
      userid = "0";
    }
    this.ngxLoader.start();
    let url = "api/UserAccount/MicroFlowSubscriptions?id=" + userid + "&productid=" + value;
    this.service.getAPI(url).then(resp => {
      // let severityHighRiskTab: any[] = [];
      // severityHighRiskTab=resp;
      this.dataSource = new MatTableDataSource(resp);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.ngxLoader.stop();
    });
  };

  onSelectRisk(row:any) {
    let obj: any = {
      data: row,
    };
    // this.cookie.put("partno",obj.data.PartNumber);
    localStorage.setItem("partno",obj.data.PartNumber);
    this.service.setData(obj);

     if (localStorage.getItem("UserId") == null) {
      this.router.navigate(["/usercarddetails"]);
    }
  else {
    this.ngxLoader.start();
    let url="api/UserAccount/GetSubscriptionId";
      var postData = {
          UserEmail: localStorage.getItem("UserEmail"),
          ProductId: this.val,
          AccessToken: localStorage.getItem("token"),
          PartNumber: obj.data.PartNumber,
          CustomerName: localStorage.getItem("UserEmail"),
          ContactEmail: localStorage.getItem("UserEmail"),
          ActivationDate: new Date(),
          TestApp: "Test123",
          UserType: localStorage.getItem("UserType"),
      }
      this.service.insertSubscription(url,postData).subscribe(resp => {
        if(resp.status=='SUCCESS')
        {
          localStorage.setItem("Redirect", "ViewPage")
      this.notification.openSnackBar("Subscription subscribed successfully","","success-snackbar");
      this.router.navigate(['/microflowproducts'])
      this.ngxLoader.stop();
        }
        else if(resp.status=='FAILED')
        {
          this.notification.openSnackBar(resp.message,"","warning-snackbar");
          this.ngxLoader.stop();
        }
      },(error)=>{
        this.notification.openSnackBar(error.message,"","red-snackbar");
        this.ngxLoader.stop();
      });
      
  } 
}
CreateUser()
{

  var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                    if (!this.email.match(validRegex)) {
                      this.notification.openSnackBar("Please enter the valid email.","","red-snackbar");
                        return;
                    }
    this.ngxLoader.start();
    let urlreq="api/UserAccount/CheckUser";
    var postData = {                   
       UserEmail: this.email
    }
    this.service.post(urlreq,postData).subscribe(resp => {
      if(resp==true)
      {
    this.notification.openSnackBar("Email already exist","","warning-snackbar");
    this.ngxLoader.stop();
    return;
      }
      else{
        this.createusernew=true;
      }

    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    }
    );






    let url = "api/UserAccount/MicroFlowSubscriptions?id=0"+"&productid=" + this.products[0].Id;
    this.service.getAPI(url).then(resp => {
      this.dataSource = new MatTableDataSource(resp);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    });

    
}
onSelectRow(row:any) {
  let obj: any = {
    data: row,
  };
  this.ngxLoader.start();
  let url="api/UserAccount/GetSubscriptionId";
    var postData = {
        UserEmail: this.email,
        ProductId: this.products[0].Id,
        AccessToken: localStorage.getItem("token"),
        PartNumber: obj.data.PartNumber,
        CustomerName: this.email,
        ContactEmail: this.email,
        ActivationDate: new Date(),
        TestApp: "Test123",
        UserType: this.usertype,
    }
    this.service.insertSubscription(url,postData).subscribe(resp => {
      if(resp.status=='SUCCESS')
      {
        this.email="";
        this.createusernew=false;
        this.ngxLoader.stop();
    this.notification.openSnackBar("Subscription subscribed successfully","","success-snackbar");
    this.router.navigate(['/microflowproducts'])

      }
      else if(resp.status=='FAILED')
      {
        this.createusernew=false;
        this.ngxLoader.stop();
        this.email="";
        this.notification.openSnackBar(resp.message,"","warning-snackbar");
      }
    },(error)=>{
      this.createusernew=false;
      this.ngxLoader.stop();
      this.notification.openSnackBar(error.message,"","red-snackbar");
    });
    
}
// Assign Spl Eval to Users
AssignEval()
{ 
  this.createusernew=false;
  this.ngxLoader.start();
  let url = "api/UserAccount/GetUsers?token=" + localStorage.getItem("token");
  this.service.getAPI(url).then(resp => {
    if(resp.status=='SUCCESS')
    {
      if (resp.userlist!=null && resp.userlist.length > 0) {
        this.ngxLoader.stop();
      return this.Users=resp.userlist;
      }
      else {
      this.Users = [];
      this.ngxLoader.stop();
      this.notification.openSnackBar("users not found.","","warning-snackbar");
    }

    }
    else if (resp.status == "FAILED") {
      this.ngxLoader.stop();
      this.notification.openSnackBar(resp.message,"","red-snackbar");

  }
  },(error)=>
  {
    this.ngxLoader.stop();
    this.notification.openSnackBar(error.message,"","red-snackbar");
  });
}
AddPages()
{
  this.ngxLoader.start();
let url="api/UserAccount/AssignEvalToInternalUser";
       let postData={
        UserEmail: this.selectedValue.UserEmail,
                                UserId: this.selectedValue.Id,
                                Name: "Eval",
                                Description: "SplEval Subscription",
                                PartNumber: "INVAB_S_EVAL_1",
                                ProductId: 1,
                                Duration: "",
                                PageAllotment: this.pagecount,
                                MaxOverage: "",
                                SubscriptionTypeCode: "Eval"
       }
       this.service.post(url,postData).subscribe(resp=>{
if(resp.status=='SUCCESS')
{
  this.ngxLoader.stop();
  this.pagecount="";
this.notification.openSnackBar("Page count added successfully.","","success-snackbar");
}
else if(resp.status=='FAILED')
  {
    this.ngxLoader.stop();
this.notification.openSnackBar(resp.message,"","warning-snackbar");
  }
},(error)=>{
  this.ngxLoader.stop();
  this.notification.openSnackBar(error.message,"","red-snackbar");
});
}
Close()
{
this.createusernew=false;
}
}
