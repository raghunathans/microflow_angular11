import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/_api/api.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { NotificationService } from 'src/app/notification.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

//  import { CookieService } from 'ngx-cookie';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})

export class CreateAccountComponent implements OnInit {
  // email:string="";
  isbtnvisible:boolean = false;
  username:any;
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
      
  }
  constructor(
     private router:Router,
     private service:ApiService,
public snackBar:MatSnackBar,
public notification:NotificationService,
private ngxLoader:NgxUiLoaderService,

  ) { }

  ngOnInit(): void {
    // this.nav.show();
    //  this.nav.doSomethingElseUseful();
    
   localStorage.removeItem("UserId");
   localStorage.removeItem("UserType");

  }
   onClickNext() {
     if(this.email.status=='INVALID')
     {
       return;
     }
    if (this.email.value=="") {
      this.notification.openSnackBar("Please enter the email address","","red-snackbar");
      // alert("Please enter email address");
      return;
    }
    //  var encrypted = this.service.encryptText('encryptemail', this.email.value);
    //  var dec = this.service.decryptText('encryptemail', encrypted);
     this.service.setCoockie(this.email.value);
     localStorage.setItem('Email',this.email.value);

     this.ngxLoader.start();
    let url="api/UserAccount/CheckUser";
      var postData = {                   
         UserEmail: this.email.value
      }
      this.service.post(url,postData).subscribe(resp => {
        if(resp==true)
        {
      this.notification.openSnackBar("Email already exist","","warning-snackbar");
      this.ngxLoader.stop();
        }
        else
        {
          localStorage.setItem("UserEmail", this.email.value);
          localStorage.setItem("Type", "User");
          this.router.navigate(['/microflowproducts'])
          this.ngxLoader.stop();
        }
      },(error)=>{
        this.notification.openSnackBar(error.message,"","red-snackbar");
        this.ngxLoader.stop();
      });


    // this.router.navigate(['/microflowproducts'])
   }
  
 Back()
 {
  this.router.navigate(['/login'])
 }
}

