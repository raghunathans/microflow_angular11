export class Product {
    Id:number;
    ProductName:string;
    Description:string;
    Discontinued:boolean;

    constructor()
    {
        this.Id=0;
        this.ProductName="";
        this.Description="";
        this.Discontinued=false;
    }
}
