import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './pages/login/login.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { MicroFlowProductsComponent } from './pages/micro-flow-products/micro-flow-products.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { SetPasswordComponent } from './pages/set-password/set-password.component';
import { SuccessComponent } from './pages/success/success.component';
import { UserCardDetailsComponent } from './pages/user-card-details/user-card-details.component';

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {path: 'login',component: LoginComponent,},
  
  {
    path: "",
    component: MenuComponent,
    children: [
      {path: 'createaccount',component: CreateAccountComponent,},
      {path: 'microflowproducts',component:MicroFlowProductsComponent,},
      {path: 'usercarddetails',component:UserCardDetailsComponent},
      {path: 'success',component:SuccessComponent,},
      {path: 'setpassword',component:SetPasswordComponent,},
      {path: 'menu',component:MenuComponent,},
      {path: 'changepassword',component:ChangePasswordComponent,},

    ],
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
